﻿using System;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace SmartAdorner
{
	public class DragThumb : Thumb
	{
		public double X
		{
			get => (double)GetValue(XProperty);
			set => SetValue(XProperty, value);
		}
		public static readonly DependencyProperty XProperty =
			 DependencyProperty.Register(nameof(X), typeof(double), typeof(DragThumb), new FrameworkPropertyMetadata(0.0d, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.Journal));

		public double Y
		{
			get => (double)GetValue(YProperty);
			set => SetValue(YProperty, value);
		}
		public static readonly DependencyProperty YProperty =
			 DependencyProperty.Register(nameof(Y), typeof(double), typeof(DragThumb), new FrameworkPropertyMetadata(0.0d, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault | FrameworkPropertyMetadataOptions.Journal));

		public double MinX
		{
			get => (double)GetValue(MinXProperty);
			set => SetValue(MinXProperty, value);
		}
		public static readonly DependencyProperty MinXProperty =
			 DependencyProperty.Register(nameof(MinX), typeof(double), typeof(DragThumb), new PropertyMetadata(0.0d));

		public double MinY
		{
			get => (double)GetValue(MinYProperty);
			set => SetValue(MinYProperty, value);
		}
		public static readonly DependencyProperty MinYProperty =
			 DependencyProperty.Register(nameof(MinY), typeof(double), typeof(DragThumb), new PropertyMetadata(0.0d));

		public double MaxX
		{
			get => (double)GetValue(MaxXProperty);
			set => SetValue(MaxXProperty, value);
		}
		public static readonly DependencyProperty MaxXProperty =
			 DependencyProperty.Register(nameof(MaxX), typeof(double), typeof(DragThumb), new PropertyMetadata(double.MaxValue));

		public double MaxY
		{
			get => (double)GetValue(MaxYProperty);
			set => SetValue(MaxYProperty, value);
		}
		public static readonly DependencyProperty MaxYProperty =
			 DependencyProperty.Register(nameof(MaxY), typeof(double), typeof(DragThumb), new PropertyMetadata(double.MaxValue));

		public DragThumb()
		{
			DragDelta += DragThumb_DragDelta;
		}

		void DragThumb_DragDelta(object sender, DragDeltaEventArgs e)
		{
			X = Math.Min(MaxX, Math.Max(MinX, X + e.HorizontalChange));
			Y = Math.Min(MaxY, Math.Max(MinY, Y + e.VerticalChange));
		}
	}
}
