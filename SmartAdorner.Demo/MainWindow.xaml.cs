﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls.Primitives;

namespace SmartAdorner.Demo
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			Icons = new ObservableCollection<Object>();
			for (int i = 0; i < 100; i++)
			{
				var icon = new IconViewModel() { Text = "Icon " + (i + 1), X = (i % 10) * 100 + 10, Y = (i / 10) * 120 + 10 };
				Icons.Add(icon);
			}

			Icons.Add(new RectViewModel() { X = 223, Y = 120, Width = 150, Height = 100 });

			Icons.Add(new LineViewModel() { X1 = 10, Y1 = 20, X2 = 100, Y2 = 100 });

			DataContext = this;
		}

		public ObservableCollection<Object> Icons { get; private set; }

		private void IconThumb_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
		{
			Thumb thumb = (Thumb)sender;
			IconViewModel icon = (IconViewModel)thumb.DataContext;
			icon.X += e.HorizontalChange;
			icon.Y += e.VerticalChange;
		}
	}
}
