﻿using System.ComponentModel;

namespace SmartAdorner.Demo
{
    public class IconViewModel : INotifyPropertyChanged
    {
        private bool m_isSelected;
        public bool IsSelected
        {
            get => m_isSelected;
            set { m_isSelected = value; OnPropertyChanged("IsSelected"); }
        }
        
        private double m_X;
        public double X
        {
            get => m_X;
            set { m_X = value; OnPropertyChanged("X"); }
        }

        private double m_Y;
        public double Y
        {
            get => m_Y;
            set { m_Y = value; OnPropertyChanged("Y"); }
        }

        private string m_text;
        public string Text
        {
            get => m_text;
            set { m_text = value; OnPropertyChanged("Text"); }
        }
        
        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}

