﻿namespace SmartAdorner.Demo
{
    /// <summary>
    /// 线条 VM
    /// </summary>
	public class LineViewModel : ListBoxItemBaseViewModel
	{
        public double X1 { get; set; }
        public double Y1 { get; set; }
        public double X2 { get; set; }
        public double Y2 { get; set; }
    }
}
