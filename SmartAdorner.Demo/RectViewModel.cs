﻿namespace SmartAdorner.Demo
{
	/// <summary>
	/// 矩形 VM
	/// </summary>
	public class RectViewModel : ListBoxItemBaseViewModel
	{
		public double X { get; set; }
		public double Y { get; set; }
		public double Width { get; set; }
		public double Height { get; set; }
	}
}
