﻿namespace SmartAdorner.Demo
{
	public class ListBoxItemBaseViewModel
	{
		/// <summary>
		/// 是否选中
		/// </summary>
		public bool IsSelected { get; set; }
	}
}
